import React from "react";
import "../App.scss";

function TaskList(props) {
  const tasks = props.tasks;
  const taskList = tasks.map((task) => {
    return (
      <li key={task.key}>
        <h2>
          {task.text}
          <span>
            <button onClick={() => props.deleteTask(task.key)}>Remove</button>
          </span>
        </h2>
      </li>
    );
  });
  return <div>{taskList}</div>;
}
export default TaskList;
