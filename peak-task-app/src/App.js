import React from "react";
import TaskList from "./components/TaskList";
import Modal from "react-modal";
import "./App.scss";

class App extends React.Component {
  constructor(props) {
    super(props);
    // defines application states
    this.state = {
      modelisOpen: false,
      randomTask: "",
      tasks: [],
      currentTask: {
        text: "",
        key: "",
      },
    };
    this.taskInputHandler = this.taskInputHandler.bind(this);
    this.addTask = this.addTask.bind(this);
    this.deleteTask = this.deleteTask.bind(this);
    this.DeleteAll = this.DeleteAll.bind(this);
    this.PickRandom = this.PickRandom.bind(this);
  }

  taskInputHandler(e) {
    this.setState({
      currentTask: {
        text: e.target.value,
        key: Date.now(),
      },
    });
  }
  //adds task to task array
  addTask(e) {
    e.preventDefault();
    const newTask = this.state.currentTask;
    if (newTask.text != "") {
      const newTasks = [...this.state.tasks, newTask];
      this.setState({
        tasks: newTasks,
        currentTask: {
          text: "",
          key: "",
        },
      });
    }
  }
  //deletes selected task
  deleteTask(key) {
    const filteredTasks = this.state.tasks.filter((task) => task.key !== key);
    this.setState({
      tasks: filteredTasks,
    });
  }
  //delets all tasks
  DeleteAll(e) {
    e.preventDefault();
    this.setState({
      tasks: [],
    });
  }
  //picks random task from array and displays on modal
  PickRandom(e) {
    const allTasks = this.state.tasks;
    const numberOfTasks = allTasks.length;
    if (numberOfTasks !== 0) {
      const randomNumber = Math.floor(Math.random() * numberOfTasks);
      const randomTask = allTasks[randomNumber].text;

      e.preventDefault();
      this.setState({
        modelisOpen: true,
        randomTask: randomTask,
      });
    } else {
      alert("Please add some tasks!");
    }
  }

  render() {
    return (
      <div className="content">
        <button className="random" onClick={this.PickRandom}>
          What Should I Do?
        </button>
        <h3>
          Options
          <span>
            <button onClick={this.DeleteAll}>Remove All</button>
          </span>
        </h3>
        <ul>
          {/* taskList component for displaying individual tasks */}
          <TaskList
            tasks={this.state.tasks}
            deleteTask={this.deleteTask}
          ></TaskList>
        </ul>
        <form onSubmit={this.addTask}>
          <input
            type="text"
            placeholder="Enter New Task"
            value={this.state.currentTask.text}
            onChange={this.taskInputHandler}
            maxLength="30"
          />
          <button type="submit">Add Option</button>
        </form>
        {/* modal for displaying random tasks */}
        <Modal
          className="modal"
          isOpen={this.state.modelisOpen}
          ariaHideApp={false}
        >
          <p>{this.state.randomTask}</p>
          <button onClick={() => this.setState({ modelisOpen: false })}>
            Ok
          </button>
        </Modal>
      </div>
    );
  }
}

export default App;
